// Coment
/*
   Comment
   Comment
*/

import os
import arrays

fn add(x int,y int) int {
	return x+y
}

fn foo() (int,int) {
	return 2,3
}

struct MyPoint {
mut:
    x int
    y int
}

struct MyLine {
	p1 MyPoint
	p2 MyPoint
}

type SumT= MyLine | MyPoint  

enum Color {
	red
	blue
	green
}

fn isred(c Color) bool {
	if c==.red {
		return true
	}
	else {
		return false
	}
}

fn myfun(mut p MyPoint)  {
	p.x= 1000
}

struct Foo {
	a int
mut: 
	b int
pub mut:
	c int
}

fn myfun2(p &MyPoint){ //pass by reference

}

fn mypanic(s string){
	panic(s)  // panics
}

type Filter = fn(string) string
fn filter(s string, f Filter) string {
	return f(s)
}


fn main() {
	a,b:=foo()
	println(a)
	println(b)
	println(os.args)
	println(add(3,5))
	println('Hello World')
	name:='Bob'  //declaration  & initialisation
	age:=20
	number:=i64(999999999)
	println(name)
	println(age)
	println(number)
	mut age2:=20  //mutable
	println(age2)
	age2=21
	println(age2)
	s := age2.str() //conversion
	println(s)
	i := s.int()
	println(i) //conversion
	l:=i64(999999999)
	println(l)
	aa:=5
	println('AA ${aa} BB') //string interpolation
	if  aa==5  {
		println(aa)
	}
	else {
		println("other")
	}
	x1:=i8(1)
	println(x1)
	x2:=i16(1)
	println(x2)
	x3:=int(1)
	println(x3)
	f1:=f32(1.0)
	println(f1)
	f2:=f64(1.0)
	println(f2)
	ss1:="RUNES".runes() //runes-u32
	ss2:=ss1.string()
	ss3:=ss2.bytes()
	println(ss1)
	println(ss2)
	println(ss3)
	namex:="Bob"
	namey:=namex[1..3]
	println(namex)
	println(namey)
	assert(namey=="ob")
	sa1:="Hello"
	arr:=s.bytes()
	sa2:=arr.bytestr()
	println(sa1)
	println(arr)
	println(sa2)
	println(sa1[0].ascii_str())
	s4:=r"Hello \kaka World "  // backlash in string
	println(s4)
	st1:="Alain"
	st2:="De Vos"
	st3:=st1 + st2
	println(st1)
	println(st2)
	println(st3)
	xxx:=123.456
	println("[${int(xxx):010}]")
	println("Data "+ xxx.str())
	println("Data ${xxx}")

	mut nums:=[1,2,3]
	println(nums)
	nums[1]=5
	println(nums[1])
	nums << 4
	nums << [5,6]
	println(nums)
	println(nums.len)
	println(nums.cap)
	mut arrb:=[u8(1),2,3,4]
	println(arrb)
	mut arrc:=[]int{len:5,cap:5,init:0}
	println(arrc)
	println(arrc)

	mut mylist :=[]SumT{}
	mylist << MyPoint{1,1}
	mylist << MyLine {
		p1 : MyPoint{2,2}
		p2 : MyPoint{3,3}
	}
	names:=["Alain","Eddy"]
	mapped:=names.map(it.to_lower())
	println(mapped)
	filtered:=mapped.filter(fn (x string) bool  {
		return x=="alain"
	} )
	println(filtered)

	mut m:=map[string]int{}
	m["one"]=1
	m["two"]=2
	m.delete("two")
	println(m["one"])
	bb:='one' in m 
	println(bb)
	mut m2:={'one' : 1
	         'two' : 2
	}
	println(m2)

	match true {
		2<2 {println("Smaller")}
		2==2 {println("Equal")}
		else {println("Other")}
	}
	numsz :=[1,2,3]
	println(1 in numsz)
	mapz:={ 'one' :  1}
	println('one' in mapz)
	println('one' in mapz)

	for ii,num in numsz {
		println("${ii}:${num}")
	}

	ppp:=&MyPoint{10,100} //heap
	println(ppp.x)
	pppp:=MyPoint{10,100}
	println(pppp.x)
	
	my_int:=1
	my_closure:=fn [my_int] (){
		println(my_int)
	}
	my_closure()

}
